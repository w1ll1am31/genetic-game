package com.project.geneticrepository.repository;

import com.project.geneticrepository.model.RocketPositions;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RocketRepository extends JpaRepository<RocketPositions, String> {
}
