package com.project.geneticrepository.repository;

import com.project.geneticrepository.model.GameRequest;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface GameRequestRepository extends JpaRepository<GameRequest, String> {
}
