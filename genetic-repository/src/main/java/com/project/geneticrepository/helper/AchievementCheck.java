package com.project.geneticrepository.helper;

import com.project.geneticrepository.model.GameRequest;
import com.project.geneticrepository.model.User;

import java.io.Serializable;

@FunctionalInterface
public interface AchievementCheck extends Serializable {
    boolean achievementMatch(User user, GameRequest gameRequest);
}
