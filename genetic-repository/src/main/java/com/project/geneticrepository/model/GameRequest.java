package com.project.geneticrepository.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.NoArgsConstructor;
import processing.core.PVector;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;
import java.util.Objects;

@Data
@Entity
@NoArgsConstructor
public class GameRequest implements Serializable {

    @Id
    private String uuid;
    private PVector startPoint;
    private PVector endPoint;
    private int endDiameter;
    private int rocketDiameter;
    private int populationSize;
    private int generationCount;
    private int lifespan;
    private int maxForce;
    private int environmentWidth;
    private int environmentHeight;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "gameRequest")
    private List<Obstacle> obstacles;

    @ManyToOne
    @JoinColumn
    private User user;

    @OneToOne(cascade = CascadeType.ALL)
    private GameStatus gameStatus;

    @OneToOne(cascade = CascadeType.ALL)
    private Result result;

    public GameRequest(@JsonProperty String uuid,
                       @JsonProperty PVector startPoint,
                       @JsonProperty PVector endPoint,
                       @JsonProperty List<Obstacle> obstacles,
                       @JsonProperty int populationSize,
                       @JsonProperty int generationCount,
                       @JsonProperty int lifespan,
                       @JsonProperty int maxForce) {
        this.uuid = uuid;
        this.startPoint = startPoint;
        this.endPoint = endPoint;
        this.obstacles = obstacles;
        this.populationSize = populationSize;
        this.generationCount = generationCount;
        this.lifespan = lifespan;
        this.maxForce = maxForce;
    }

    @Override
    public String toString() {
        return "GameRequest{" +
                "uuid='" + uuid + '\'' +
                ", startPoint=" + startPoint +
                ", endPoint=" + endPoint +
                ", populationSize=" + populationSize +
                ", generationCount=" + generationCount +
                ", lifespan=" + lifespan +
                ", maxForce=" + maxForce +
                ", obstaclesSize=" + obstacles.size() +
                ", gameStatusUuid=" + gameStatus.getUuid() +
                ", resultUuid=" + result.getUuid() +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        GameRequest that = (GameRequest) o;
        return populationSize == that.populationSize &&
                generationCount == that.generationCount &&
                lifespan == that.lifespan &&
                maxForce == that.maxForce &&
                Objects.equals(uuid, that.uuid) &&
                Objects.equals(startPoint, that.startPoint) &&
                Objects.equals(endPoint, that.endPoint);
    }

    @Override
    public int hashCode() {
        return Objects.hash(uuid, startPoint, endPoint, populationSize, generationCount, lifespan, maxForce);
    }
}
