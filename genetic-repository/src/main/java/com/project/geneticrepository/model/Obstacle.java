package com.project.geneticrepository.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;

@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor
public class Obstacle implements Serializable {
    @Id
    private String uuid;
    private int x;
    private int y;
    private int width;
    private int height;

    @ManyToOne
    @JoinColumn(name = "gameRequest_uuid")
    @JsonIgnore
    private GameRequest gameRequest;

    public Obstacle(int x, int y, int width, int height) {
        this.x = x;
        this.y = y;
        this.width = width;
        this.height = height;
    }

}
