package com.project.geneticrepository.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import processing.core.PVector;

import javax.persistence.*;
import java.util.List;

@Data
@Entity
public class RocketPositions {
    @Id
    private String uuid;

    @ElementCollection
    private List<PVector> positions;

    private String colour;

    @ManyToOne
    @JoinColumn
    @JsonIgnore
    private Generation generation;
}
