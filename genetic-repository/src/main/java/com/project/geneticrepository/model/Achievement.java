package com.project.geneticrepository.model;

import com.project.geneticrepository.helper.AchievementCheck;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Achievement {
    private String name;
    private String description;
    private AchievementCheck achievementCheck;
}
