package com.project.geneticrepository.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import processing.core.PVector;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class DNA {
    private List<PVector> genes;
    private int maxForce;

    public DNA(int lifespan, int maxForce) {
        this.maxForce = maxForce;
        genes = new ArrayList<>();
        for(int i = 0; i < lifespan; i++) {
            genes.add(PVector.random2D().setMag(maxForce*-1));
        }
    }

    public DNA(DNA dna1, DNA dna2) {
        this.genes = mergeDNA(dna1, dna2);
    }

    private List<PVector> mergeDNA(DNA dna1, DNA dna2) {
        try {
//            Random rand = new Random();
//            int midPoint = rand.nextInt(dna1.getGenes().size());
            int midPoint = dna1.getGenes().size()/2;
            List<PVector> newGenes = new ArrayList<>(dna1.getGenes().subList(0, midPoint));
            newGenes.addAll(dna2.getGenes().subList(midPoint, dna2.getGenes().size()));
            return newGenes;
        } catch (Exception e) {
            throw e;
        }
    }

}
