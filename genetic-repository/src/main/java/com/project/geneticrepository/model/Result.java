package com.project.geneticrepository.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor
public class Result implements Serializable {
    @Id
    private String uuid;

    @OneToMany(cascade = CascadeType.ALL)
    private List<Generation> generations;

    @OneToOne
    @JoinColumn
    @JsonIgnore
    private GameRequest gameRequest;

    public void addGeneration(Generation generation) {
        if(this.generations == null) {
            this.generations = new ArrayList<>();
        }
        this.generations.add(generation);
    }

    @Override
    public String toString() {
        return "Result{" +
                "uuid='" + uuid + '\'' +
                ", generationCount=" + generations.size() +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Result result = (Result) o;
        return Objects.equals(uuid, result.uuid) &&
                Objects.equals(generations, result.generations);
    }

    @Override
    public int hashCode() {
        return Objects.hash(uuid, generations);
    }
}
