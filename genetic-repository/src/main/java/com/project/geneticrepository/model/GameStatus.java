package com.project.geneticrepository.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.Value;

import javax.persistence.*;
import java.io.Serializable;
import java.time.Instant;
import java.time.LocalDateTime;
import java.util.Objects;

@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor
public class GameStatus implements Serializable {
    @Id
    private String uuid;
    private String state;
    private String stage;
    private long timeSubmitted;

    @Transient
    private String requestUuid;
    @Transient
    private int currentGeneration;
    @Transient
    private int generationCount;

    @OneToOne
    @JsonIgnore
    private GameRequest gameRequest;

    public GameStatus(String uuid, String state, String stage, GameRequest gameRequest) {
        this.uuid = uuid;
        this.state = state;
        this.stage = stage;
        this.gameRequest = gameRequest;
        this.timeSubmitted = Instant.now().getEpochSecond();
    }

    @Override
    public String toString() {
        return "GameStatus{" +
                "uuid='" + uuid + '\'' +
                ", state='" + state + '\'' +
                ", stage='" + stage + '\'' +
                ", requestUuid='" + requestUuid + '\'' +
                ", currentGeneration=" + currentGeneration +
                ", generationCount=" + generationCount +
                ", gameRequestUuid=" + gameRequest.getUuid() +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        GameStatus that = (GameStatus) o;
        return currentGeneration == that.currentGeneration &&
                generationCount == that.generationCount &&
                Objects.equals(uuid, that.uuid) &&
                Objects.equals(state, that.state) &&
                Objects.equals(stage, that.stage) &&
                Objects.equals(requestUuid, that.requestUuid);
    }

    @Override
    public int hashCode() {
        return Objects.hash(uuid, state, stage, requestUuid, currentGeneration, generationCount);
    }
}
