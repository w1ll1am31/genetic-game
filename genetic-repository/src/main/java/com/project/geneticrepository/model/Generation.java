package com.project.geneticrepository.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Data
@Entity
@AllArgsConstructor
@NoArgsConstructor
public class Generation {
    @Id
    private String uuid;

    private int generationPosition;

    @ManyToOne
    @JoinColumn
    @JsonIgnore
    private Result result;

    @OneToMany(cascade = CascadeType.ALL)
    private List<RocketPositions> rocketPositions;

    public void addRocket(RocketPositions rocket) {
        if(this.rocketPositions == null) {
            this.rocketPositions = new ArrayList<>();
        }
        this.rocketPositions.add(rocket);
    }

    @Override
    public String toString() {
        return "Generation{" +
                "uuid='" + uuid + '\'' +
                ", rocketPositionsSize=" + rocketPositions.size() +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Generation that = (Generation) o;
        return Objects.equals(uuid, that.uuid) &&
                Objects.equals(rocketPositions, that.rocketPositions);
    }

    @Override
    public int hashCode() {
        return Objects.hash(uuid, rocketPositions);
    }
}

