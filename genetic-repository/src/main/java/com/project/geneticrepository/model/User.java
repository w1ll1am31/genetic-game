package com.project.geneticrepository.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Data
@Entity
@AllArgsConstructor
@NoArgsConstructor
public class User implements Serializable {
    @Id
    private String uuid;

    @Column(nullable = false, unique = true)
    private String username;

    private String password;
    private String role;
    private boolean active;

    @OneToMany(cascade = CascadeType.ALL)
    @JsonIgnore
    private List<GameRequest> gameRequestList;

    @ElementCollection
    private List<String> userAchievementIdList;

    @Transient
    private List<Achievement> achievements;

    public User(String uuid, String username, String encodedPassword, String role) {
        this.uuid = uuid;
        this.username = username;
        this.password = encodedPassword;
        this.role = role;
        this.gameRequestList = new ArrayList<>();
        this.achievements = new ArrayList<>();
        this.active = false;
    }

    public void addGameRequest(GameRequest gameRequest) {
        if(this.gameRequestList == null) {
            this.gameRequestList = new ArrayList<>();
        }
        this.gameRequestList.add(gameRequest);
    }
}
