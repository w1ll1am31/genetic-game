package com.project.geneticrepository.model;

import lombok.Data;
import processing.core.PVector;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Data
public class Population {
    private List<Rocket> rocketList;

    public Population(GameRequest gameRequest) {
        rocketList = new ArrayList<>();
        for(int i = 0; i < gameRequest.getPopulationSize(); i++) {
            rocketList.add(new Rocket(gameRequest.getLifespan(), gameRequest.getMaxForce(), gameRequest.getStartPoint()));
        }
    }

    public Generation run(PVector endPoint, List<Obstacle> obstacleList, int environmentWidth, int environmentHeight, int endDiameter, int rocketDiameter) {
        Generation generation = new Generation();
        generation.setUuid(UUID.randomUUID().toString());
        for(Rocket rocket : rocketList) {
            RocketPositions rocketPositions = new RocketPositions();
            rocketPositions.setPositions(rocket.fly(endPoint, obstacleList, environmentWidth, environmentHeight, endDiameter, rocketDiameter));
            rocketPositions.setUuid(UUID.randomUUID().toString());
            rocketPositions.setGeneration(generation);
            if(rocket.isBestFromLast()) {
                rocketPositions.setColour("yellow");
            } else {
                rocketPositions.setColour("blue");
            }
            generation.addRocket(rocketPositions);
        }
        return generation;
    }
}
