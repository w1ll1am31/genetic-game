package com.project.geneticrepository.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import processing.core.PVector;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Rocket {

    private DNA dna;
    private PVector position;
    private PVector velocity;
    private PVector acceleration;
    private PVector startPosition;
    private float fitness;
    private boolean crashed;
    private boolean arrived;
    private boolean bestFromLast;

    private static final Logger LOGGER = LoggerFactory.getLogger(Rocket.class);

    public Rocket(DNA newDna, PVector startPoint) {
        this.dna = newDna;
        this.position = startPoint.copy();
        this.velocity = new PVector();
        this.acceleration = new PVector();
        this.startPosition = startPoint.copy();
        this.fitness = 0;
        this.crashed = false;
        this.arrived = false;
        this.bestFromLast = false;
    }


    public Rocket(int lifespan, int maxForce, PVector startPoint) {
        this.dna = new DNA(lifespan, maxForce);
        this.position = startPoint.copy();
        this.velocity = new PVector();
        this.acceleration = new PVector();
        this.startPosition = startPoint.copy();
        this.fitness = 0;
        this.crashed = false;
        this.arrived = false;
        this.bestFromLast = false;
    }

    public List<PVector> fly(PVector endPoint, List<Obstacle> obstacleList, int environmentWidth, int environmentHeight, int endDiameter, int rocketDiameter) {
        resetData();
        List<PVector> positions = new ArrayList<>();
        for (PVector gene : this.dna.getGenes()) {
            this.acceleration.add(gene);
            this.velocity.add(this.acceleration);
            this.position.add(this.velocity);
            this.acceleration.mult(0);

            if (this.position.x <= 0 || this.position.x >= environmentWidth || this.position.y <= 0 || this.position.y >= environmentHeight) {
                this.crashed = true;
            } else if (PVector.dist(this.position, endPoint) <= (rocketDiameter/2) + (endDiameter/2)) {
                LOGGER.debug("Hit the target");
                this.arrived = true;
            } else {
                for (Obstacle obstacle : obstacleList) {
                    if (this.hitsObstacle(obstacle)) {
                        this.crashed = true;
                        break;
                    }
                }
            }
            if (this.crashed || this.arrived) {
                positions.addAll(Collections.nCopies(this.dna.getGenes().size() - positions.size(), this.position.copy()));
                break;
            }
            positions.add(this.position.copy());
        }
        calculateFitness(endPoint);
        return positions;
    }

    private void resetData() {
        this.position = this.startPosition.copy();
        this.acceleration.mult(0);
        this.velocity.mult(0);
        this.crashed = false;
        this.arrived = false;
        this.fitness = 0;
    }

    private boolean hitsObstacle(Obstacle obstacle) {
        return (this.position.x >= obstacle.getX()) &&
                (this.position.x <= (obstacle.getX() + obstacle.getWidth())) &&
                (this.position.y >= obstacle.getY()) &&
                (this.position.y <= obstacle.getY() + obstacle.getHeight());
    }

    private void calculateFitness(PVector endPoint) {
//        float dist = PVector.dist(this.position, endPoint);
//        fitness = 100 - (int)(dist * 100 / 2000);
//        if(this.crashed) {
//            fitness /= 10;
//        } else if(this.arrived) {
//            fitness *= 10;
//        }
//        float distFromEnd = PVector.dist(this.position, endPoint);
//        fitness = Math.round(100 * (distFromEnd/(1000)));

        float distance = PVector.dist(this.position, endPoint);
        if(this.arrived) {
            this.fitness = 1;
        } else {
            this.fitness = 1 / distance;
            if (this.crashed) {
                this.fitness /= 10;
            }
        }
    }
}
