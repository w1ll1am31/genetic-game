package com.project.geneticrepository.model;

import org.junit.Test;
import processing.core.PVector;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static org.junit.Assert.*;

public class RocketTest {

    @Test
    public void testFlyPositionsChange() {
        Rocket rocket = setupRocket(10);

        List<PVector> rocketPositions = rocket.fly(new PVector(100, 100), Collections.emptyList(), 800, 800, 20, 20);

        assertNotEquals(rocketPositions.get(0), rocketPositions.get(1));
        assertTrue(rocketPositions.get(0).y < rocketPositions.get(9).y);
        assertTrue(rocket.getFitness() > 0);
    }

    @Test
    public void testWhenRocketHitsGoalItStopsAndFitnessIsHigh() {
        Rocket rocket = setupRocket(20);
        PVector end = new PVector(10, 50);

        List<PVector> rocketPositions = rocket.fly(end, Collections.emptyList(), 800, 800, 20,  20);

        assertTrue(rocket.isArrived());
        assertTrue(PVector.dist(end, rocketPositions.get(rocketPositions.size() - 1)) < 30);
        assertEquals(1, rocket.getFitness(), 0);
    }

    private Rocket setupRocket(int lifespan) {
        Rocket rocket = new Rocket(lifespan, 1, new PVector(10, 0));
        List<PVector> genes = new ArrayList<>();
        for(int i = 0; i < lifespan; i ++) {
            genes.add(new PVector(0, 10));
        }
        DNA dna = new DNA(genes, 1);
        rocket.setDna(dna);
        return rocket;
    }

    @Test
    public void testRocketHitsObstacleAndStops() {
        Rocket rocket = setupRocket(20);
        List<PVector> rocketPositions = rocket.fly(new PVector(10, 100), Collections.singletonList(new Obstacle(5, 50, 20, 10)), 800, 800, 20, 20);

        assertTrue(rocket.isCrashed());
        assertTrue(rocket.getFitness() < 0.5);
    }
}