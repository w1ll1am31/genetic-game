package com.project.geneticmaster.exception;

public class UserExistsException extends Exception {
    public UserExistsException(String msg) {
        super(msg);
    }
}
