package com.project.geneticmaster;

import com.project.geneticrepository.helper.AchievementCheck;
import com.project.geneticrepository.model.Achievement;
import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;

import java.util.HashMap;
import java.util.Map;

@Data
@Configuration
@EnableAspectJAutoProxy
@ConfigurationProperties("genetic.master")
public class GeneticMasterConfiguration {
    private String exchangeName;
    private Map<String, Achievement> achievementList = createAchievementList();
    private String defaultUsername;
    private String defaultPassword;

    private Map<String, Achievement> createAchievementList() {
        Map<String, Achievement> achievementMap = new HashMap<>();
        Achievement achievement = new Achievement();
        achievement.setName("First job sent");
        achievement.setDescription("Congratulations! You've sent of your first test");
        AchievementCheck check = (user, gameRequest) -> user.getGameRequestList().size() == 1;
        achievement.setAchievementCheck(check);

        achievementMap.put("1", achievement);
        achievementMap.put("2",
                new Achievement("First time logon",
                        "Logon to the application for the first time!",
                        (u, r) -> !u.isActive()));
        return achievementMap;
    }
}
