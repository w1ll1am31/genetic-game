package com.project.geneticmaster.security;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class NewUserRequest {
    private String username;
    private String password1;
    private String password2;
    private boolean isAdmin;
}
