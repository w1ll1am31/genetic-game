package com.project.geneticmaster.service;

import com.project.geneticmaster.GeneticMasterConfiguration;
import com.project.geneticmaster.exception.PasswordMismatchException;
import com.project.geneticmaster.exception.UserExistsException;
import com.project.geneticmaster.security.*;
import com.project.geneticmaster.web.GameStatusResponse;
import com.project.geneticrepository.model.*;
import com.project.geneticrepository.repository.GameRequestRepository;
import com.project.geneticrepository.repository.GameStatusRepository;
import com.project.geneticrepository.repository.UserRepository;
import javassist.NotFoundException;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.time.Instant;
import java.util.*;

@Service
public class GeneticMasterService {

    private final GameRequestRepository gameRequestRepository;
    private final GameStatusRepository gameStatusRepository;
    private final RabbitTemplate rabbitTemplate;
    private final GeneticMasterConfiguration geneticMasterConfiguration;
    private final UserRepository userRepository;
    private final PasswordEncoder passwordEncoder;

    @Autowired
    public GeneticMasterService(GameRequestRepository gameRequestRepository,
                                GameStatusRepository gameStatusRepository,
                                RabbitTemplate rabbitTemplate,
                                GeneticMasterConfiguration geneticMasterConfiguration,
                                UserRepository userRepository,
                                PasswordEncoder passwordEncoder) {
        this.gameRequestRepository = gameRequestRepository;
        this.gameStatusRepository = gameStatusRepository;
        this.rabbitTemplate = rabbitTemplate;
        this.geneticMasterConfiguration = geneticMasterConfiguration;
        this.userRepository = userRepository;
        this.passwordEncoder = passwordEncoder;
    }

    public String sendGameRequest(GameRequest gameRequest, UserPrincipal userPrincipal) {
        gameRequest.setUuid(UUID.randomUUID().toString());
        User user = userRepository.findByUsername(userPrincipal.getUsername()).orElseThrow(() -> new UsernameNotFoundException("No user with name: " + userPrincipal.getUsername()));
        GameStatus gameStatus = new GameStatus(UUID.randomUUID().toString(), "Started", "Request", gameRequest);
        gameRequest.setUser(user);

        if(gameRequest.getObstacles() == null) {
            gameRequest.setObstacles(new ArrayList<>());
        }
        gameRequest.getObstacles().forEach(o -> {
            o.setUuid(UUID.randomUUID().toString());
            o.setGameRequest(gameRequest);
        });
        user.addGameRequest(gameRequest);
        userRepository.saveAndFlush(user);
        gameRequest.setGameStatus(gameStatus);
        gameRequestRepository.save(gameRequest);
        rabbitTemplate.convertAndSend(geneticMasterConfiguration.getExchangeName(), "newGeneticGameRequest", gameRequest);
        return gameRequest.getUuid();
    }

    public Optional<GameStatusResponse> getGameStatus(String uuid) {
        return gameRequestRepository.findById(uuid).map(r -> {
            GameStatus status = r.getGameStatus();
            return new GameStatusResponse(Instant.ofEpochSecond(status.getTimeSubmitted()).toString(), r.getUser().getUsername(), status.getState(), status.getStage(), r.getUuid(), status.getCurrentGeneration(), status.getGenerationCount());
        });
    }

    public Optional<GameRequest> getGameForUuid(String uuid) {
        Optional<GameRequest> gameRequest = gameRequestRepository.findById(uuid);
        gameRequest.ifPresent(r -> r.getResult().getGenerations().sort(Comparator.comparingInt(Generation::getGenerationPosition)));
        return gameRequest;
    }

    public List<GameStatusResponse> getAllGames() {
        List<GameStatus> gameStatusList = gameStatusRepository.findAll();
        List<GameStatusResponse> gameStatusResponseList = new ArrayList<>();

        gameStatusList.forEach(s -> {
            GameStatusResponse statusResponse = gameStatusToGameStatusResponse(s);
            gameStatusResponseList.add(statusResponse);
        });
        return gameStatusResponseList;
    }

    private GameStatusResponse gameStatusToGameStatusResponse(GameStatus gameStatus) {
        GameStatusResponse response = new GameStatusResponse();
        GameRequest request = gameStatus.getGameRequest();
        response.setRequestUuid(gameStatus.getGameRequest().getUuid());
        response.setGenerationCount(gameStatus.getGameRequest().getGenerationCount());
        Result result = gameStatus.getGameRequest().getResult();
        if(result != null && result.getGenerations() != null) {
            response.setCurrentGeneration(result.getGenerations().size());
        } else {
            response.setCurrentGeneration(1);
        }
        response.setTimeSubmitted(Instant.ofEpochSecond(gameStatus.getTimeSubmitted()).toString());
        response.setUsername(gameStatus.getGameRequest().getUser().getUsername());
        response.setState(gameStatus.getState());
        response.setStage(gameStatus.getStage());
        return response;
    }

    public List<GameStatusResponse> getGamesForUser(UserPrincipal userPrincipal) {
        User user = getUserByUsername(userPrincipal.getUsername());
        List<GameRequest> gameRequestList = user.getGameRequestList();
        List<GameStatusResponse> gameStatusResponseList = new ArrayList<>();
        gameRequestList
                .forEach(r -> {
                    gameStatusResponseList.add(gameStatusToGameStatusResponse(r.getGameStatus()));
                });
        return gameStatusResponseList;
    }

    public void registerNewUser(NewUserRequest newUserRequest) throws UserExistsException, PasswordMismatchException {
        if(!newUserRequest.getPassword1().equals(newUserRequest.getPassword2())) {
            throw new PasswordMismatchException("Passwords do not match");
        }
        String role;
        if(newUserRequest.isAdmin()) {
            role = "ROLE_ADMIN";
        } else {
            role = "ROLE_USER";
        }
        User user = new User(UUID.randomUUID().toString(), newUserRequest.getUsername(), passwordEncoder.encode(newUserRequest.getPassword1()), role);
        boolean userExists = userRepository.findByUsername(user.getUsername()).isPresent();
        if(userExists) {
            throw new UserExistsException("User with name " + user.getUsername() + " already exists");
        }
        userRepository.saveAndFlush(user);
    }

    public void setUserRole(String username, String role) {
        User user = userRepository.findByUsername(username).orElseThrow(() -> new UsernameNotFoundException("User " + username + " not found"));
        user.setRole(role);
        userRepository.saveAndFlush(user);
    }

    public List<User> getAllUsers() {
        return userRepository.findAll();
    }

    public void deleteUser(String username) {
        User user = userRepository.findByUsername(username).orElseThrow(() -> new UsernameNotFoundException("User: " + username +" not found"));
        userRepository.delete(user);
    }

    public User getUserByUsername(String username) {
        User user = userRepository.findByUsername(username).orElseThrow(() -> new UsernameNotFoundException("User: " + username +" not found"));
        List<Achievement> achievementList = new ArrayList<>();
        user.getUserAchievementIdList()
                .forEach(id -> achievementList.add(geneticMasterConfiguration.getAchievementList().get(id)));
        user.setAchievements(achievementList);
        return user;
    }

    public Achievement activateUser(UserPrincipal userPrincipal) {
        User user = getUserByUsername(userPrincipal.getUsername());
        if(!user.isActive()) {
            user.setActive(true);
            user.getUserAchievementIdList().add("2");
            userRepository.saveAndFlush(user);
            return geneticMasterConfiguration.getAchievementList().get("2");
        }
        return null;
    }

    public void deleteTest(String uuid) throws NotFoundException {
        GameRequest gameRequest = gameRequestRepository.findById(uuid).orElseThrow(() -> new NotFoundException("request with ID " + uuid + " not found"));
        gameStatusRepository.delete(gameRequest.getGameStatus());
        User user = gameRequest.getUser();
        user.getGameRequestList().remove(gameRequest);
        userRepository.save(user);
        gameRequestRepository.delete(gameRequest);
    }
}
