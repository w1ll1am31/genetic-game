package com.project.geneticmaster.web;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class GameStatusResponse {
    private String timeSubmitted;
    private String username;
    private String state;
    private String stage;
    private String requestUuid;
    private int currentGeneration;
    private int generationCount;
}
