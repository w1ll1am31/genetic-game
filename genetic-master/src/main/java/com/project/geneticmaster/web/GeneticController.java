package com.project.geneticmaster.web;

import com.project.geneticmaster.security.NewUserRequest;
import com.project.geneticmaster.security.UserPrincipal;
import com.project.geneticmaster.security.UserRoles;
import com.project.geneticmaster.service.GeneticMasterService;
import com.project.geneticrepository.model.Achievement;
import com.project.geneticrepository.model.GameRequest;
import com.project.geneticrepository.model.GameStatus;
import com.project.geneticrepository.model.User;
import javassist.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;

@RestController
@CrossOrigin(origins = "http://localhost:8081")
@RequestMapping(value = "/api")
public class GeneticController {

    private final GeneticMasterService geneticMasterService;

    @Autowired
    public GeneticController(GeneticMasterService geneticMasterService) {
        this.geneticMasterService = geneticMasterService;
    }


    @PostMapping(value = "/startGame")
    public ResponseEntity<String> sendRequest(@RequestBody GameRequest request, Authentication authentication) {
        UserPrincipal userPrincipal = (UserPrincipal)authentication.getPrincipal();
        String uuid = geneticMasterService.sendGameRequest(request, userPrincipal);
        return new ResponseEntity<>(uuid, HttpStatus.ACCEPTED);
    }

    @GetMapping(value = "/getStatus/{uuid}")
    public ResponseEntity<GameStatusResponse> getGameStatus(@PathVariable String uuid) {
        return ResponseEntity.of(geneticMasterService.getGameStatus(uuid));
    }

    @GetMapping(value = "/getResults/{uuid}")
    public ResponseEntity<GameRequest> getResults(@PathVariable String uuid) {
        return ResponseEntity.of(geneticMasterService.getGameForUuid(uuid));
    }

    @GetMapping(value = "/getAllGames")
    public ResponseEntity<List<GameStatusResponse>> getAllGames(Authentication authentication) {
        UserPrincipal userPrincipal = (UserPrincipal)authentication.getPrincipal();
        if(userPrincipal.getAuthorities().contains(new SimpleGrantedAuthority("ROLE_ADMIN"))) {
            return ResponseEntity.ok(geneticMasterService.getAllGames());
        } else {
            return ResponseEntity.ok(geneticMasterService.getGamesForUser(userPrincipal));
        }
    }

    @PostMapping(value = "/admin/registerNewUser")
    public ResponseEntity<String> registerNewUser(@RequestBody NewUserRequest newUserRequest) {
        try {
            geneticMasterService.registerNewUser(newUserRequest);
            return new ResponseEntity<>(newUserRequest.getUsername(), HttpStatus.CREATED);
        } catch (Exception e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.CONFLICT);
        }
    }

    @PostMapping(value = "/admin/setUserRole/{username}", consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Void> setUserRole(@RequestBody HashMap<String, String> role, @PathVariable(value = "username") String username) {
        try {
            geneticMasterService.setUserRole(username, role.get("role"));
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (UsernameNotFoundException e) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping(value = "/getUser")
    public ResponseEntity<User> getUser(Authentication authentication) {
        UserPrincipal userPrincipal = (UserPrincipal) authentication.getPrincipal();
        User user = geneticMasterService.getUserByUsername(userPrincipal.getUsername());
        return ResponseEntity.ok(user);
    }

    @GetMapping(value = "/admin/getAllUsers")
    public ResponseEntity<List<User>> getAllUsers() {
        List<User> allUsers = geneticMasterService.getAllUsers();
        return ResponseEntity.ok(allUsers);
    }

    @DeleteMapping(value = "/admin/deleteUser/{username}")
    public ResponseEntity<Void> deleteUser(@PathVariable String username) {
        try {
            geneticMasterService.deleteUser(username);
            return ResponseEntity.noContent().build();
        } catch (UsernameNotFoundException e) {
          return ResponseEntity.notFound().build();
        }
    }

    @DeleteMapping(value = "/admin/deleteTest/{uuid}")
    public ResponseEntity<Void> deleteTest(@PathVariable String uuid) {
        try {
            geneticMasterService.deleteTest(uuid);
            return ResponseEntity.noContent().build();
        } catch (NotFoundException e) {
            return ResponseEntity.notFound().build();
        }
    }

    @PostMapping(value = "/activateUser")
    public ResponseEntity<Achievement> activateUser(Authentication authentication) {
        UserPrincipal userPrincipal = (UserPrincipal) authentication.getPrincipal();
        Achievement achievement = geneticMasterService.activateUser(userPrincipal);
        if(achievement != null) {
            return ResponseEntity.ok(achievement);
        }
        return ResponseEntity.noContent().build();
    }
}
