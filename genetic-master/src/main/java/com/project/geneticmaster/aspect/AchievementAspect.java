package com.project.geneticmaster.aspect;

import com.project.geneticmaster.GeneticMasterConfiguration;
import com.project.geneticmaster.security.UserPrincipal;
import com.project.geneticmaster.service.GeneticMasterService;
import com.project.geneticrepository.model.Achievement;
import com.project.geneticrepository.model.GameRequest;
import com.project.geneticrepository.model.User;
import com.project.geneticrepository.repository.UserRepository;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Aspect
@Component
public class AchievementAspect {

    private static final Logger LOGGER = LoggerFactory.getLogger(AchievementAspect.class);
    private final GeneticMasterService geneticMasterService;
    private final GeneticMasterConfiguration geneticMasterConfiguration;
    private final UserRepository userRepository;

    @Autowired
    public AchievementAspect(GeneticMasterService geneticMasterService,
                             GeneticMasterConfiguration geneticMasterConfiguration, UserRepository userRepository) {
        this.geneticMasterConfiguration = geneticMasterConfiguration;
        this.geneticMasterService = geneticMasterService;
        this.userRepository = userRepository;
    }

    @Around("execution(public org.springframework.http.ResponseEntity com.project.geneticmaster.web.GeneticController.*(..))")
    public Object aroundModifier(ProceedingJoinPoint proceedingJoinPoint) throws Throwable {
        Object obj = proceedingJoinPoint.proceed();
        UserPrincipal userPrincipal = (UserPrincipal) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        User user = geneticMasterService.getUserByUsername(userPrincipal.getUsername());
        GameRequest gameRequest;
        List<Achievement> newAchievements = new ArrayList<>();
        for(Object arg : proceedingJoinPoint.getArgs()) {
            if (arg instanceof GameRequest) {
                gameRequest =  (GameRequest) arg;
                geneticMasterConfiguration.getAchievementList().entrySet().stream()
                        .filter(a -> !user.getUserAchievementIdList().contains(a.getKey()))
                        .filter(a -> a.getValue().getAchievementCheck().achievementMatch(user, gameRequest))
                        .forEach(a -> {
                            user.getUserAchievementIdList().add(a.getKey());
                            userRepository.saveAndFlush(user);
                            newAchievements.add(a.getValue());
                        });
                break;
            }
        }

        ResponseEntity<?> responseEntity = (ResponseEntity<?>) obj;
        Object body = responseEntity.getBody();
        Map<String, Object> newReturn = new HashMap<>();
        newReturn.put("body", body);
        newReturn.put("achievements", newAchievements);

        return new ResponseEntity<Object>(newReturn, responseEntity.getStatusCode());
    }
}
