package com.project.geneticmaster;

import com.project.geneticmaster.security.NewUserRequest;
import com.project.geneticmaster.service.GeneticMasterService;
import com.project.geneticrepository.helper.AchievementCheck;
import com.project.geneticrepository.model.Achievement;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

import javax.annotation.PostConstruct;
import java.util.HashMap;
import java.util.UUID;

@EnableJpaRepositories(basePackages = {"com.project.geneticrepository.repository"})
@EntityScan(basePackages = {"com.project.geneticrepository.model"})
@SpringBootApplication
public class GeneticMasterApplication {
    public static void main(String[] args) {
        SpringApplication.run(GeneticMasterApplication.class, args);
    }
}

