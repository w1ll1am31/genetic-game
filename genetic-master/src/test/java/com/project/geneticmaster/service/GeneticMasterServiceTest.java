package com.project.geneticmaster.service;

import com.project.geneticmaster.GeneticMasterConfiguration;
import com.project.geneticmaster.exception.PasswordMismatchException;
import com.project.geneticmaster.exception.UserExistsException;
import com.project.geneticmaster.security.NewUserRequest;
import com.project.geneticmaster.security.UserPrincipal;
import com.project.geneticmaster.web.GameStatusResponse;
import com.project.geneticrepository.model.User;
import com.project.geneticrepository.repository.UserRepository;
import com.project.geneticmaster.security.UserRoles;
import com.project.geneticrepository.model.GameRequest;
import com.project.geneticrepository.model.GameStatus;
import com.project.geneticrepository.model.Generation;
import com.project.geneticrepository.model.Result;
import com.project.geneticrepository.repository.GameRequestRepository;
import com.project.geneticrepository.repository.GameStatusRepository;
import com.project.geneticrepository.repository.ResultRepository;
import javassist.NotFoundException;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.test.context.junit4.SpringRunner;
import processing.core.PVector;

import java.time.Instant;
import java.util.*;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

@RunWith(SpringRunner.class)
@DataJpaTest
@EntityScan(basePackages = {"com.project.geneticrepository.model", "com.project.geneticmaster.security"})
@ComponentScan(basePackages = {"com.project.geneticmaster", "com.project.geneticrepository.repository"})
public class GeneticMasterServiceTest {

    @Autowired
    private GeneticMasterService geneticMasterServiceUnderTest;
    @Autowired
    private GameRequestRepository gameRequestRepository;
    @Autowired
    private GameStatusRepository gameStatusRepository;
    @Autowired
    private ResultRepository resultRepository;
    @MockBean
    private RabbitTemplate rabbitTemplate;
    @Autowired
    private GeneticMasterConfiguration geneticMasterConfiguration;
    @Autowired
    private UserRepository userRepository;

    private User user;

    @Before
    public void setup() {
        userRepository.deleteAll();
        user = new User(UUID.randomUUID().toString(), "newUser", "password", "ROLE_ADMIN");
        userRepository.saveAndFlush(user);
    }

    @Test
    public void testRequestReceivedPersistedAndSentToRabbit() {
        GameRequest newGameRequest = new GameRequest(null, new PVector(0, 0), new PVector(100, 100), null, 10, 10, 10, 1);
        UserPrincipal userPrincipal = new UserPrincipal(user);
        doNothing().when(rabbitTemplate).convertAndSend(geneticMasterConfiguration.getExchangeName(), "newGeneticGameRequest", newGameRequest);

        String requestUuid = geneticMasterServiceUnderTest.sendGameRequest(newGameRequest, userPrincipal);
        Optional<GameRequest> savedRequest = gameRequestRepository.findById(requestUuid);
        if (!savedRequest.isPresent()) {
            fail();
        }

        assertNotNull(savedRequest.get().getGameStatus());
        verify(rabbitTemplate, times(1)).convertAndSend(geneticMasterConfiguration.getExchangeName(), "newGeneticGameRequest", newGameRequest);

    }

    @Test
    public void testGetGameStatusByGameRequestUuid() throws NotFoundException {
        String requestUuid = UUID.randomUUID().toString();
        GameRequest gameRequest = new GameRequest(requestUuid, null, null, null, 10, 10, 10, 1);
        gameRequest.setUser(user);
        user.addGameRequest(gameRequest);
        GameStatus gameStatus = new GameStatus(UUID.randomUUID().toString(), "Started", "Request", 0, requestUuid, 0, 0, gameRequest);
        GameStatusResponse gameStatusResponse = new GameStatusResponse(Instant.ofEpochSecond(gameStatus.getTimeSubmitted()).toString(), gameRequest.getUser().getUsername(), gameStatus.getState(), gameStatus.getStage(), requestUuid, gameStatus.getCurrentGeneration(), gameStatus.getGenerationCount());
        gameRequest.setGameStatus(gameStatus);

        gameRequestRepository.saveAndFlush(gameRequest);

        GameStatusResponse actual = geneticMasterServiceUnderTest.getGameStatus(requestUuid).orElseThrow(() -> new NotFoundException("No Status"));

        assertEquals(gameStatusResponse, actual);

    }

    @Test
    public void testGetGameForUuid() throws NotFoundException {
        String requestUuid = UUID.randomUUID().toString();
        GameRequest expected = new GameRequest(requestUuid, null, null, Collections.emptyList(), 10, 10, 10, 1);
        Generation generation = new Generation(UUID.randomUUID().toString(), 1, null, Collections.emptyList());
        Result result = new Result(UUID.randomUUID().toString(), Collections.singletonList(generation), expected);
        generation.setResult(result);
        expected.setResult(result);

        gameRequestRepository.saveAndFlush(expected);

        GameRequest actual = geneticMasterServiceUnderTest.getGameForUuid(requestUuid).orElseThrow(() -> new NotFoundException("No Request"));

        assertEquals(expected, actual);
    }

    @Test
    public void getAllGames() {
        GameRequest gameRequest1 = new GameRequest(UUID.randomUUID().toString(), null, null, Collections.emptyList(), 10, 10, 10, 1);
        gameRequest1.setUser(user);
        user.addGameRequest(gameRequest1);
        GameRequest gameRequest2 = new GameRequest(UUID.randomUUID().toString(), null, null, Collections.emptyList(), 10, 10, 10, 1);
        gameRequest2.setUser(user);
        user.addGameRequest(gameRequest2);
        GameStatus gameStatus1 = new GameStatus(UUID.randomUUID().toString(), "Started", "Request", 0, gameRequest1.getUuid(), 2, 10, gameRequest1);
        GameStatus gameStatus2 = new GameStatus(UUID.randomUUID().toString(), "Started", "Request", 0, gameRequest2.getUuid(), 2, 10, gameRequest2);

        GameStatusResponse gameStatusResponse1 = new GameStatusResponse("1970-01-01T00:00:00Z", user.getUsername(), gameStatus1.getState(), gameStatus1.getStage(), gameRequest1.getUuid(), gameStatus1.getCurrentGeneration(), gameStatus1.getGenerationCount());
        GameStatusResponse gameStatusResponse2 = new GameStatusResponse("1970-01-01T00:00:00Z", user.getUsername(), gameStatus2.getState(), gameStatus2.getStage(), gameRequest2.getUuid(), gameStatus2.getCurrentGeneration(), gameStatus2.getGenerationCount());

        gameRequest1.setGameStatus(gameStatus1);
        gameRequest2.setGameStatus(gameStatus2);

        Generation generation1 = new Generation(UUID.randomUUID().toString(), 0, null, Collections.emptyList());
        Generation generation2 = new Generation(UUID.randomUUID().toString(), 1, null, Collections.emptyList());
        Result result1 = new Result(UUID.randomUUID().toString(), Arrays.asList(generation1, generation2), gameRequest1);
        generation1.setResult(result1);
        generation2.setResult(result1);
        gameRequest1.setResult(result1);

        Generation generation3 = new Generation(UUID.randomUUID().toString(), 2, null, Collections.emptyList());
        Generation generation4 = new Generation(UUID.randomUUID().toString(), 3, null, Collections.emptyList());
        Result result2 = new Result(UUID.randomUUID().toString(), Arrays.asList(generation3, generation4), gameRequest2);
        generation3.setResult(result2);
        generation4.setResult(result2);
        gameRequest2.setResult(result2);

        gameRequestRepository.saveAndFlush(gameRequest1);
        gameRequestRepository.saveAndFlush(gameRequest2);

        List<GameStatusResponse> actual = geneticMasterServiceUnderTest.getAllGames();

        assertEquals(2, actual.size());
        assertEquals(gameStatusResponse1, actual.get(0));
        assertEquals(gameStatusResponse2, actual.get(1));

        assertEquals(gameRequest1.getUuid(), actual.get(0).getRequestUuid());
    }

    @Test
    public void testAddNewUser() throws UserExistsException, PasswordMismatchException {
        NewUserRequest newUserRequest = new NewUserRequest("User1", "password", "password", false);
        geneticMasterServiceUnderTest.registerNewUser(newUserRequest);

        User expected = new User(UUID.randomUUID().toString(), "User1", "pass1", "ROLE_USER");
        Optional<User> actual = userRepository.findByUsername("User1");
        assertTrue(actual.isPresent());
        assertEquals(expected.getUsername(), actual.get().getUsername());
        assertEquals(expected.getRole(), actual.get().getRole());
    }

    @Test
    public void testAddExistingUserThrowsException() {
        NewUserRequest newUserRequest = new NewUserRequest("User1", "password", "password", false);
        try {
            geneticMasterServiceUnderTest.registerNewUser(newUserRequest);
        } catch (Exception e) {
            fail();
        }

        try {
            geneticMasterServiceUnderTest.registerNewUser(newUserRequest);
            fail();
        } catch (Exception ignored) {
        }
    }

    @Test(expected = PasswordMismatchException.class)
    public void testUserRequestMustHaveTwoSamePasswords() throws UserExistsException, PasswordMismatchException {
        NewUserRequest newUserRequest = new NewUserRequest("User1", "password1", "password2", false);
        geneticMasterServiceUnderTest.registerNewUser(newUserRequest);
        fail("User accepted with different passwords");
    }

    @Test
    public void testMakeUserAdmin() throws UserExistsException, PasswordMismatchException {
        NewUserRequest newUserRequest = new NewUserRequest("User1", "password", "password", false);
        geneticMasterServiceUnderTest.registerNewUser(newUserRequest);

        User expected = new User(UUID.randomUUID().toString(), "User1", "pass1", "ROLE_USER");
        Optional<User> actual = userRepository.findByUsername("User1");
        assertTrue(actual.isPresent());
        assertEquals(expected.getUsername(), actual.get().getUsername());
        assertEquals(expected.getRole(), actual.get().getRole());

        geneticMasterServiceUnderTest.setUserRole(newUserRequest.getUsername(), "ROLE_ADMIN");

        expected.setRole("ROLE_ADMIN");

        actual = userRepository.findByUsername("User1");
        assertTrue(actual.isPresent());
        assertEquals(expected.getUsername(), actual.get().getUsername());
        assertEquals(expected.getRole(), actual.get().getRole());
    }
}