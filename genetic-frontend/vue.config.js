module.exports = {
  outputDir: 'target/dist',
  assetsDir: 'static',
    devServer: {
        proxy: {
            '/': {
                target: 'http://localhost:8080',
                ws: false,
                changeOrigin: true
            }
        }
    },
};