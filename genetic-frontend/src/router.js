import Vue from 'vue'
import Router from 'vue-router'
import HelloWorld from './pages/HelloWorld'
import Leaderboard from './pages/Leaderboard'
import NewGame from './pages/NewGame'
import Admin from './pages/Admin'

Vue.use(Router);

export default new Router({
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      name: 'home',
      component: HelloWorld
    },
    {
      path: '/leaderboard',
      name: 'leaderboard',
      component: Leaderboard
    },
    {
      path: '/newGame',
      name: 'newGame',
      component: NewGame
    }
  ]
})
