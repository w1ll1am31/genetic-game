minikube start

kubectl apply -f ./deployments/rabbitmq.yml
kubectl apply -f ./deployments/genetic-master.yml
kubectl apply -f ./deployments/genetic-worker.yml