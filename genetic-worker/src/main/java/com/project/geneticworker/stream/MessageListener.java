package com.project.geneticworker.stream;

import com.project.geneticrepository.model.GameRequest;
import com.project.geneticrepository.model.GameStatus;
import com.project.geneticrepository.repository.GameStatusRepository;
import com.project.geneticworker.service.GeneticWorkerService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.annotation.Exchange;
import org.springframework.amqp.rabbit.annotation.Queue;
import org.springframework.amqp.rabbit.annotation.QueueBinding;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class MessageListener {

    private static final Logger LOGGER = LoggerFactory.getLogger(MessageListener.class);

    private final GameStatusRepository gameStatusRepository;
    private final GeneticWorkerService geneticWorkerService;

    @Autowired
    public MessageListener(GameStatusRepository gameStatusRepository, GeneticWorkerService geneticWorkerService) {
        this.gameStatusRepository = gameStatusRepository;
        this.geneticWorkerService = geneticWorkerService;
    }

    @RabbitListener(bindings = {
            @QueueBinding(
                    value = @Queue(name = "${genetic.worker.queueName}"),
                    exchange = @Exchange(name = "${genetic.worker.exchangeName}", type = "topic"),
                    key = "#")
    })
    public void receiveNewGameRequest(GameRequest gameRequest) {
        GameStatus gameStatus = gameRequest.getGameStatus();
        gameStatus.setStage("Processing");
        gameStatus.setState("Started");
        if(!gameStatusRepository.findById(gameStatus.getUuid()).isPresent()) {
            LOGGER.error("Status not found for uuid {}", gameStatus.getUuid());
        } else {
            gameStatusRepository.save(gameStatus);
            LOGGER.info(gameRequest.getUuid());
            geneticWorkerService.startNewTest(gameRequest);
        }
    }
}
