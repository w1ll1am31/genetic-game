package com.project.geneticworker.service;

import com.project.geneticrepository.model.*;
import com.project.geneticrepository.repository.GameRequestRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.retry.annotation.Backoff;
import org.springframework.retry.annotation.Retryable;
import org.springframework.stereotype.Service;
import processing.core.PVector;

import java.util.*;

@Service
public class GeneticWorkerService {

    private final GameRequestRepository gameRequestRepository;
    private static final Logger LOGGER = LoggerFactory.getLogger(GeneticWorkerService.class);

    @Autowired
    public GeneticWorkerService(GameRequestRepository gameRequestRepository) {
        this.gameRequestRepository = gameRequestRepository;
    }

    @Retryable(maxAttempts = 4, backoff = @Backoff(delay = 500))
    public void startNewTest(GameRequest gameRequest) {
        /**
         * create a population with x rockets in
         * each rocket will have a dna of length y
         *
         */

        Population population = new Population(gameRequest);
        Result result = new Result();
        result.setUuid(UUID.randomUUID().toString());
        result.setGameRequest(gameRequest);
        gameRequest.setResult(result);
        for (int i = 0; i < gameRequest.getGenerationCount(); i++) {
            Generation generation = population.run(gameRequest.getEndPoint(), gameRequest.getObstacles(), gameRequest.getEnvironmentWidth(), gameRequest.getEnvironmentHeight(), gameRequest.getEndDiameter(), gameRequest.getRocketDiameter());
            generation.setGenerationPosition(i);
            generation.setResult(result);
            result.addGeneration(generation);
            gameRequestRepository.save(gameRequest);
            LOGGER.debug("Finished generation {}", i);
            population.setRocketList(evolve(population.getRocketList(), gameRequest));
        }
        gameRequest.getGameStatus().setState("Finished");
        gameRequestRepository.save(gameRequest);
    }

    private List<Rocket> evolve(List<Rocket> rockets, GameRequest gameRequest) {
        List<DNA> matingPool = new ArrayList<>();
        Rocket bestRocket = rockets.stream().max(Comparator.comparing(Rocket::getFitness)).get();
        Rocket worstRocket = rockets.stream().min(Comparator.comparing(Rocket::getFitness)).get();

        for (Rocket rocket : rockets) {
            // map fitness which is between best and worst to range of 2 -> 100
            // mapping from 2 will ensure that there is at least populationSize*2 entries in the mating pool
            int rocketsInPool = Math.round(((rocket.getFitness()-worstRocket.getFitness())/(bestRocket.getFitness()-worstRocket.getFitness()))*98)+2;
            matingPool.addAll(Collections.nCopies(rocketsInPool, rocket.getDna()));
        }
        Random random = new Random();
        List<Rocket> newRockets = new ArrayList<>();
        for (int i = 0; i < gameRequest.getPopulationSize()-1; i++) {
            DNA dna1 = matingPool.get(random.nextInt(matingPool.size()));
            DNA dna2 = matingPool.get(random.nextInt(matingPool.size()));
            DNA newDna = new DNA(dna1, dna2);
            newRockets.add(new Rocket(newDna, gameRequest.getStartPoint()));
            matingPool.remove(dna1);
            matingPool.remove(dna2);
        }
        for (Rocket rocket : newRockets) {
            mutate(rocket, gameRequest.getMaxForce());
        }

        bestRocket.setBestFromLast(true);
        newRockets.add(bestRocket);

        return newRockets;
    }

    private void mutate(Rocket rocket, int maxForce) {
        Random random = new Random();
        List<PVector> mutatedGenes = new ArrayList<>();
        for (PVector gene : rocket.getDna().getGenes()) {
            if (random.nextInt(100) > 90) {
                PVector newGene = PVector.random2D();
                newGene.setMag(maxForce);
                mutatedGenes.add(newGene);
            } else {
                mutatedGenes.add(gene);
            }
        }
        rocket.getDna().setGenes(mutatedGenes);
    }
}
