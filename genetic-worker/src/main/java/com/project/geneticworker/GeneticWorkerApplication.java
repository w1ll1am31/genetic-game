package com.project.geneticworker;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@EnableJpaRepositories(basePackages = {"com.project.geneticrepository.repository"})
@EntityScan(basePackages = {"com.project.geneticrepository.model"})
@SpringBootApplication
public class GeneticWorkerApplication {

    public static void main(String[] args) {
        SpringApplication.run(GeneticWorkerApplication.class, args);
    }

}
