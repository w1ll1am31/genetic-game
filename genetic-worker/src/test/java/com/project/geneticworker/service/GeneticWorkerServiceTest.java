package com.project.geneticworker.service;

import com.project.geneticrepository.model.GameRequest;
import com.project.geneticrepository.model.GameStatus;
import com.project.geneticrepository.repository.GameRequestRepository;
import com.project.geneticrepository.repository.ResultRepository;
import javassist.NotFoundException;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.test.context.junit4.SpringRunner;
import processing.core.PVector;

import java.util.Collections;
import java.util.UUID;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

@RunWith(SpringRunner.class)
@DataJpaTest
@EntityScan(basePackages = "com.project.geneticrepository.model")
@ComponentScan(basePackages = {"com.project.geneticworker", "com.project.geneticrepository.repository"})
public class GeneticWorkerServiceTest {

    @Autowired
    private GeneticWorkerService geneticWorkerServiceUnderTest;
    @Autowired
    private GameRequestRepository gameRequestRepository;

    @Test
    public void testStartNewGameInitialisesTestEnvAndSavesState() throws NotFoundException {
        GameRequest gameRequest = new GameRequest(UUID.randomUUID().toString(), new PVector(0, 0), new PVector(100, 100), Collections.emptyList(), 1, 2, 1, 1);
        GameStatus gameStatus = new GameStatus(UUID.randomUUID().toString(), "Started", "Request", gameRequest);
        gameRequest.setGameStatus(gameStatus);
        gameRequestRepository.saveAndFlush(gameRequest);

        geneticWorkerServiceUnderTest.startNewTest(gameRequest);

        GameRequest updatedRequest = gameRequestRepository.findById(gameRequest.getUuid()).orElseThrow(() -> new NotFoundException("No Game Request"));

        assertNotEquals(updatedRequest.getResult().getGenerations().get(0).getRocketPositions(), updatedRequest.getResult().getGenerations().get(1).getRocketPositions());
        assertEquals("Finished", updatedRequest.getGameStatus().getState());
    }

}